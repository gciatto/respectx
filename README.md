# RespextX Development Project

RespectX empowers the [ReSpecT language](http://apice.unibo.it/xwiki/bin/view/ReSpecT/WebHome) - which remains the underlying language actually exploited for coordination by the [TuCSoN middleware](http://apice.unibo.it/xwiki/bin/view/TuCSoN/WebHome) - with a few crucial features, enhancing the language itself and adding the necessary tooling, like, for instance: 

* __modularity__: RespectX programs denitions can be split in different modules to be imported in a root specication file, enabling and promoting code reuse as well as development of code libraries (e.g. [RespectX Standard Library](https://bitbucket.org/gciatto/respectx-standard-library/overview));

* __development tools__: RespectX programs are written through an editor distributed as an [Eclipse IDE](http://www.eclipse.org/) plugin and featuring syntax highlighting, static error checking, code completion, and code generation (of ReSpecT specication files and Prolog theories);

* __syntactic sugar__: RespectX adds special guard predicates testing presence/absence of tuples without side effects (e.g. actual consumption of tuples), and adopts a more imperative style syntax for the benet of developers not familiar with declarative languages such as Prolog.

## Develpment environment setup

RespectX support is provided by means of a plugin for the [Eclipse IDE](http://www.eclipse.org/).
RespectX-enabled Eclipse eases the quick development of ReSpecT/RespectX reactions governing the interaction space.

The Eclipse plugin is developed by means of the Eclipse IDE itself and the [Xtext](https://www.eclipse.org/Xtext/) framework.

This repository contains the Eclipse projects enabling the development & deployment of the Eclipse plugins providing RespectX support to (other) Eclipse instances.

### Eclipse workspace setup

The first step is setting up your workspace.

- Dowload Eclipse Installer from [Xtext download page](https://www.eclipse.org/Xtext/download.html)
- Launch the installer, select __"Eclipse DSL Tools"__ and follow the wizard instraction to install Eclise.
- Open Eclipse selecting the cloned repository root directory as workspace
    - Such a directory will be referred as `./` in what follows
    - Similarly, `../` will refer to your workspace parent directory
- Re-import all project contained into the `./` folder
	0. File > Import... > A dialog window should appear
	0. Select the entry "Existing Projects into Workspace" within the "General" folder
	0. Click on the "Next" button
	0. Provide the absolute path of the `./` to the text field next to "Select root directory"
		+ If you typed the path, press ENTER
		+ A list of project should appear
	0. Select all appearing projects and then click on the "Finish" button
		+ The dialog should disappear and several projects should appear into the "Package Explorer" view, on the left
        + Many errors could appear. That's ok, don't worry
- Generate code for projects `prolog`, `respect` and `respectx`
    0. Select the file `prolog` > `src` > `it.unibo.gciatto.prolog` > `GenerateProlog.mwe2`
        - Right click > Run As > MWE2 Workflow
        - Click on button "Proceed"
    0. Do the same for `respect` > `src` > `it.unibo.gciatto.prolog` > `GenerateRespect.mwe2`
    0. Do the same for `respectx` > `src` > `it.unibo.gciatto.prolog` > `GenerateRespect.mwe2`
- Projects `*.tests` may still containt some errors. Ignore them, by be sure other projects don't.

## Running a RespectX-enabled Eclipse instance

A RespectX-enabled Eclipse instance is useful if you need to write some ReSpecT/__RespectX__ code.
Syntax highlighting and code completion will help you to quickly develop your coordination logic.

###  RespectX-enabled Eclipse instance from workspace
Following these instructions, you will be able to launch a ("nested") RespectX-enabled instance of the Eclipse IDE starting from your develpement environment.

- Right click on project `respectx.ui`
- From the contextual menu, select Run As > Eclipse Application
- A "nested" instance of the Eclipse IDE will now start
- Such a "nested" Eclipse instance will be Prolog/ReSpecT/__RespectX__-enabled

## Using a RespectX-enabled Eclipse instance

To exploit RespectX plugin features within Eclipse, you just need to create a new file with the `.rspx` extension within an already existing project. Of course, on a clean worspace, you will have to create a new project (File > New > Other... > General > Project > Finish).
The first time you create a `.rspx` file, you will be asked if you want to add Xtext nature to the receiving project. Say yes.

As soon as you save a well-formed `.rspx` file containing a `specification` module, Eclipse will generate the corresponding `.rsp` file for you.

If you want to creare some raw ReSpecT (resp. Prolog) file and you also want to exploit the plugins features, you just need to create some `.rsp` (resp. `.pl`) files.