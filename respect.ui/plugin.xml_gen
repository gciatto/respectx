<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.0"?>

<plugin>

    <extension
            point="org.eclipse.ui.editors">
        <editor
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="true"
            extensions="rsp"
            id="it.unibo.gciatto.respect.Respect"
            name="Respect Editor">
        </editor>
    </extension>
    <extension
        point="org.eclipse.ui.handlers">
        <handler
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
            commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
            <activeWhen>
                <reference
                    definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
        <handler
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
            commandId="it.unibo.gciatto.respect.Respect.validate">
         <activeWhen>
            <reference
                    definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
            </reference>
         </activeWhen>
      	</handler>
      	<!-- copy qualified name -->
        <handler
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName">
            <activeWhen>
				<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened" />
            </activeWhen>
        </handler>
        <handler
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName">
            <activeWhen>
            	<and>
            		<reference definitionId="it.unibo.gciatto.respect.Respect.XtextEditor.opened" />
	                <iterate>
						<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
					</iterate>
				</and>
            </activeWhen>
        </handler>
    </extension>
    <extension point="org.eclipse.core.expressions.definitions">
        <definition id="it.unibo.gciatto.respect.Respect.Editor.opened">
            <and>
                <reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="it.unibo.gciatto.respect.Respect" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
        <definition id="it.unibo.gciatto.respect.Respect.XtextEditor.opened">
            <and>
                <reference definitionId="isXtextEditorActive"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="it.unibo.gciatto.respect.Respect" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
    </extension>
    <extension
            point="org.eclipse.ui.preferencePages">
        <page
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="it.unibo.gciatto.respect.Respect"
            name="Respect">
            <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
        </page>
        <page
            category="it.unibo.gciatto.respect.Respect"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
            id="it.unibo.gciatto.respect.Respect.coloring"
            name="Syntax Coloring">
            <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
        </page>
        <page
            category="it.unibo.gciatto.respect.Respect"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
            id="it.unibo.gciatto.respect.Respect.templates"
            name="Templates">
            <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="it.unibo.gciatto.respect.Respect"
            name="Respect">
            <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension
        point="org.eclipse.ui.keywords">
        <keyword
            id="it.unibo.gciatto.respect.ui.keyword_Respect"
            label="Respect"/>
    </extension>
    <extension
         point="org.eclipse.ui.commands">
      <command
            description="Trigger expensive validation"
            id="it.unibo.gciatto.respect.Respect.validate"
            name="Validate">
      </command>
      <!-- copy qualified name -->
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
    </extension>
    <extension point="org.eclipse.ui.menus">
        <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
             <command
                 commandId="it.unibo.gciatto.respect.Respect.validate"
                 style="push"
                 tooltip="Trigger expensive validation">
            <visibleWhen checkEnabled="false">
                <reference
                    definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
                </reference>
            </visibleWhen>
         </command>  
         </menuContribution>
         <!-- copy qualified name -->
         <menuContribution locationURI="popup:#TextEditorContext?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName" 
         		style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="menu:edit?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            	style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="popup:org.eclipse.xtext.ui.outline?after=additions">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName" 
				style="push" tooltip="Copy Qualified Name">
         		<visibleWhen checkEnabled="false">
	            	<and>
	            		<reference definitionId="it.unibo.gciatto.respect.Respect.XtextEditor.opened" />
						<iterate>
							<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
						</iterate>
					</and>
				</visibleWhen>
			</command>
         </menuContribution>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
                	<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
                	</reference>
            	</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
	    <handler
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
            commandId="org.eclipse.xtext.ui.editor.FindReferences">
            <activeWhen>
                <reference
                    definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
    </extension>   

<!-- adding resource factories -->

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="rsp">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="rsp">
        </resourceServiceProvider>
    </extension>


	<!-- marker definitions for it.unibo.gciatto.respect.Respect -->
	<extension
	        id="respect.check.fast"
	        name="Respect Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.fast"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="respect.check.normal"
	        name="Respect Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.normal"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="respect.check.expensive"
	        name="Respect Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.expensive"/>
	    <persistent value="true"/>
	</extension>

   <extension
         point="org.eclipse.xtext.builder.participant">
      <participant
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.builder.IXtextBuilderParticipant"
            fileExtensions="rsp"
            >
      </participant>
   </extension>
   <extension
            point="org.eclipse.ui.preferencePages">
        <page
            category="it.unibo.gciatto.respect.Respect"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="it.unibo.gciatto.respect.Respect.compiler.preferencePage"
            name="Compiler">
            <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            category="it.unibo.gciatto.respect.Respect"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="it.unibo.gciatto.respect.Respect.compiler.propertyPage"
            name="Compiler">
            <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?after=xtext.ui.openDeclaration">
			<command
				commandId="org.eclipse.xtext.ui.OpenGeneratedFileCommand"
				id="it.unibo.gciatto.respect.Respect.OpenGeneratedCode"
				style="push">
					<visibleWhen checkEnabled="false">
						<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened" />
					</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
		<handler
			class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.generator.trace.OpenGeneratedFileHandler"
			commandId="org.eclipse.xtext.ui.OpenGeneratedFileCommand">
				<activeWhen>
					<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened" />
				</activeWhen>
		</handler>
	</extension>

	<!-- Quick Outline -->
	<extension
		point="org.eclipse.ui.handlers">
		<handler 
			class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.outline.quickoutline.ShowQuickOutlineActionHandler"
			commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline">
			<activeWhen>
				<reference
					definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
		<command
			description="Open the quick outline."
			id="org.eclipse.xtext.ui.editor.outline.QuickOutline"
			name="Quick Outline">
		</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.open">
			<command commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline"
				style="push"
				tooltip="Open Quick Outline">
				<visibleWhen checkEnabled="false">
					<reference definitionId="it.unibo.gciatto.respect.Respect.Editor.opened"/>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
    <!-- quickfix marker resolution generator for it.unibo.gciatto.respect.Respect -->
    <extension
            point="org.eclipse.ui.ide.markerResolution">
        <markerResolutionGenerator
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="it.unibo.gciatto.respect.ui.respect.check.fast">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="it.unibo.gciatto.respect.ui.respect.check.normal">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="it.unibo.gciatto.respect.ui.respect.check.expensive">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
    </extension>
   	<!-- Rename Refactoring -->
	<extension point="org.eclipse.ui.handlers">
		<handler 
			class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.DefaultRenameElementHandler"
			commandId="org.eclipse.xtext.ui.refactoring.RenameElement">
			<activeWhen>
				<reference
					definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
    <extension point="org.eclipse.ui.menus">
         <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
         <command commandId="org.eclipse.xtext.ui.refactoring.RenameElement"
               style="push">
            <visibleWhen checkEnabled="false">
               <reference
                     definitionId="it.unibo.gciatto.respect.Respect.Editor.opened">
               </reference>
            </visibleWhen>
         </command>
      </menuContribution>
   </extension>
   <extension point="org.eclipse.ui.preferencePages">
	    <page
	        category="it.unibo.gciatto.respect.Respect"
	        class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.RefactoringPreferencePage"
	        id="it.unibo.gciatto.respect.Respect.refactoring"
	        name="Refactoring">
	        <keywordReference id="it.unibo.gciatto.respect.ui.keyword_Respect"/>
	    </page>
	</extension>

  <extension point="org.eclipse.compare.contentViewers">
    <viewer id="it.unibo.gciatto.respect.Respect.compare.contentViewers"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="rsp">
    </viewer>
  </extension>
  <extension point="org.eclipse.compare.contentMergeViewers">
    <viewer id="it.unibo.gciatto.respect.Respect.compare.contentMergeViewers"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="rsp" label="Respect Compare">
     </viewer>
  </extension>
  <extension point="org.eclipse.ui.editors.documentProviders">
    <provider id="it.unibo.gciatto.respect.Respect.editors.documentProviders"
            class="it.unibo.gciatto.respect.ui.RespectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.model.XtextDocumentProvider"
            extensions="rsp">
    </provider>
  </extension>
  <extension point="org.eclipse.team.core.fileTypes">
    <fileTypes
            extension="rsp"
            type="text">
    </fileTypes>
  </extension>

</plugin>
