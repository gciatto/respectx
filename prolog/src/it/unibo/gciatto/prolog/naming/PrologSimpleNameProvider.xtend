package it.unibo.gciatto.prolog.naming

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.naming.IQualifiedNameConverter
import it.unibo.gciatto.prolog.prolog.Variable
import org.eclipse.xtext.naming.QualifiedName

class PrologSimpleNameProvider implements IPrologSimpleNameProvider {
	
	@Inject protected IQualifiedNameConverter qnc
	
	override getFullyQualifiedName(EObject obj) {
		switch(obj) {
			Variable: obj.qualifiedName
			default: obj.qualifiedName
		}	
	}
	
	def getQualifiedName(Variable t) {
		t.name.toQualifiedName
	} 
	
	def getQualifiedName(EObject obj) {
		val getNameMethod = obj.class.methods.filter[it.name == "getName"]

		if (getNameMethod.empty) {
			null
		} else {
			(getNameMethod.head.invoke(obj) as String).toQualifiedName
		}
	} 
	
	override apply(EObject input) {
		input.fullyQualifiedName
	}
	
	protected def QualifiedName toQualifiedName(CharSequence cs) {
		qnc.toQualifiedName(cs.toString)
	}
}