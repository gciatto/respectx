package it.unibo.gciatto.prolog.scoping

import it.unibo.gciatto.prolog.scoping.IContextualScopeProvider
import org.eclipse.xtext.scoping.IScope
import org.eclipse.emf.ecore.EObject

interface IPrologContextualScopeProvider extends IContextualScopeProvider {
	def IScope getVariablesInContext(EObject node)
}