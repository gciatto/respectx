package it.unibo.gciatto.prolog.scoping

import org.eclipse.xtext.scoping.IScope
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.xbase.lib.Functions.Function1

interface IContextualScopeProvider {
	def IScope getScope(EObject node)
	def EObject getContext(EObject node)
	def <T extends EObject> IScope getFilteredScope(EObject node, Class<T> type)
	def IScope getFilteredScope(EObject node, Function1<EObject, Boolean> filter)
}