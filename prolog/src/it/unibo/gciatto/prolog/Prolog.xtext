grammar it.unibo.gciatto.prolog.Prolog hidden(T_WHITE_SPACE, SL_COMMENT, ML_COMMENT)

import "http://www.eclipse.org/emf/2002/Ecore" as ecore
generate prolog "http://www.unibo.it/gciatto/prolog/Prolog"

Theory
	:	(rules+=Rule '.')+
	;
	
Rule returns Rule
	:	Expression
	;
    
Expression returns Expression
	:	QueryLike
	;
	
/**
 * fx 1200
 */
QueryLike returns Expression
	:	name=QueryLikeOperator right=DisjunctionLike
	|	RuleLike
	;
	
QueryLikeOperator returns ecore::EString
	:	':-'
	|	'?-'
	;
	
/**
 * xfx 1200
 */
RuleLike returns Expression
	:	DisjunctionLike ({ Expression.left=current } name=RuleLikeOperator right=DisjunctionLike)?
	;
	
RuleLikeOperator returns ecore::EString
	:	':-'
	;

/**
 * xfy 1100
 */
DisjunctionLike returns Expression
	:	ImplicationLike ({ Expression.left=current } name=DisjunctionLikeOperator right=DisjunctionLike)?
	;
	
DisjunctionLikeOperator returns ecore::EString
	:	';'
	;
	
/**
 * xfy 1050
 */
ImplicationLike returns Expression
	:	ConjunctionLike ({ Expression.left=current } name=ImplicationLikeOperator right=ImplicationLike)?
	;
	
ImplicationLikeOperator returns ecore::EString
	:	'->'
	;

/**
 * xfy 1000
 */
ConjunctionLike returns Expression
	:	NegationLike ({ Expression.left=current } name=ConjunctionLikeOperator right=ConjunctionLike)?
	;
	
ConjunctionLikeOperator returns ecore::EString
	:	','
	;
	
/**
 * fy 900
 */
NegationLike returns Expression
	:	name=NegationLikeOperator right=NegationLike
	|	ComparisonLike
	;
	
NegationLikeOperator returns ecore::EString
	:	'not'
	|	'\\+'
	;

/**
 * xfx 700
 */
ComparisonLike returns Expression
	:	SumLike ({ Expression.left=current } name=ComparisonLikeOperator right=SumLike)?
	;
	
ComparisonLikeOperator returns ecore::EString
	:	'<' 
	|	'='
	|	'=..' 
	|	'=@=' 
	|	'@<' 
	|	'@=<' 
	|	'@>' 
	|	'@>=' 
	|	'=:=' 
	|	'=<' 
	|	'==' 
	|	'=\\=' 
	|	'>' 
	|	'>=' 
	|	'\\=' 
	|	'\\==' 
	|	'is'
	;

/**
 * yfx 500
 */
SumLike returns Expression
	:	ProductLike ({ Expression.left=current } name=SumLikeOperator right=ProductLike)*
	;
	
SumLikeOperator returns ecore::EString
	:	'+'
	|	'-'
	|	'/\\'
	|	'\\/'
	;

/**
 * yfx 400
 */
ProductLike returns Expression
	:	OppositeLike ({ Expression.left=current } name=ProductLikeOperator right=OppositeLike)*
	;
	
ProductLikeOperator returns ecore::EString
	:	'*'
	|	'/'
	|	'//'
	|	'>>'
	|	'<<'
	|	'mod'
	|	'rem'
	;
	
/**
 * fy 200
 */
OppositeLike returns Expression
	:	name=OppositeLikeOperator right=OppositeLike
	|	PowerLike
	;
	
OppositeLikeOperator returns ecore::EString
	:	'-'
	;
	
/**
 * xfx 200
 */
PowerLike returns Expression
	:	ApixLike ({ Expression.left=current } name=PowerLikeOperator right=Expression0)?
	;
	
PowerLikeOperator returns ecore::EString
	:	'**'
	;
	
/**
 * xfy 200
 */
ApixLike returns Expression
	:	Expression0 ({ Expression.left=current } name=ApixLikeOperator right=ApixLike)?
	;
	
ApixLikeOperator returns ecore::EString
	:	'^'
	;

Expression0 returns Expression
	:	Term
	|	name='(' nested=Expression ')'
	;
   
Term returns Term
	:	Struct
	|	Number
	|	Variable
	|	List
	|	AtomKeyword
	;

Number returns Term
    :   { Number } intValue=T_INTEGER
    |   { Number } doubleValue=T_FLOAT
    ;

AtomKeyword returns Atom
	:	Cut
	|	True
	|	Fail
	;
	
Cut returns Atom
	:	{ Cut } name='!'
	;
	
True returns Atom
	:	{ True } name='true'
	;
	
Fail returns Atom
	:	{ Fail } name='fail'
	;

Struct returns Struct
    :  	name=T_ATOM '(' argument+=Expression0 (',' argument+=Expression0)* ')'
	|	Atom
    ;

Atom returns Atom
	:	name=T_ATOM
	|	AtomString
	;
    
AtomString returns Atom
	:	{ AtomString } name=STRING
	;
    

Variable returns Variable
    :	name=T_VARIABLE
    |	AnonymousVariable
    ;
    
AnonymousVariable returns Variable
	:	{ AnonymousVariable } name='_'
	;

List returns List
    :   { List } name='[' (head+=Expression0 (',' head+=Expression0)* ('|' tail=Expression0)?)? ']'
    ;
    
terminal fragment T_NATURAL
	:   ('1'..'9') ('0'..'9')* | '0'
	;
	
terminal fragment T_DIGIT_SEQ
	:	('0' .. '9')+
	;
	
terminal T_FLOAT returns ecore::EDoubleObject
	:   T_INTEGER '.' T_DIGIT_SEQ (('E' | 'e') T_INTEGER)?
	;
	
terminal T_INTEGER returns ecore::EIntegerObject
	:   ('+' | '-')? T_NATURAL
	;
	
terminal fragment T_CHARSEQ
	:	'A'..'Z' | 'a'..'z' | '0'..'9' | '_'
	;

terminal fragment T_STRING_CONTENT
	:	T_CHARSEQ | '#' | '$' | '&' | '*' | '+' | '-' | '.' | '/' | ':' | '<' | '=' | '>' | '?' | '@' | '^' | '~'| ' ' | '\t'
	;

terminal T_ATOM 
    :   ('a'..'z') T_CHARSEQ*
    |	'\'' (T_STRING_CONTENT)* '\''
    ;

terminal T_VARIABLE
    :   ('A'..'Z') T_CHARSEQ*
    |	'_' T_CHARSEQ T_CHARSEQ*
    ;

terminal STRING
    :	'"' -> '"'
    ;

terminal T_WHITE_SPACE
	:   (' ' | '\t' | '\r' | '\n')+
	;

terminal SL_COMMENT
	:	'%' !('\n'|'\r')* ('\r'? '\n')?
	;
	
terminal ML_COMMENT
	:	'/*' -> '*/'
	;