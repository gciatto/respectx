package it.unibo.gciatto.prolog.validation

import it.unibo.gciatto.prolog.prolog.Expression
import it.unibo.gciatto.prolog.prolog.PrologPackage
import it.unibo.gciatto.prolog.prolog.Struct

import static extension it.unibo.gciatto.prolog.utils.ModelUtils.*
import it.unibo.gciatto.xtext.validation.utils.ValidationException

package class DirectiveValidator {

	static class OpDirectiveNotAllowedException extends ValidationException {

		static val MSG = "Directive :-op/3 is forbidden."
		static val CODE = "opDirective"

		new() {
			super(
				Type.ERROR,
				MSG,
				PrologPackage.Literals.EXPRESSION__RIGHT,
				CODE
			)
		}

	}
	
	static val INSTANCE = new DirectiveValidator;
	
	static def get() {
		INSTANCE
	}

	def Iterable<? extends ValidationException> checkOpDirective(Expression e) {
		if (e.directive && e.right instanceof Struct && "op" == (e.right as Struct).functor &&
			3 == (e.right as Struct).arity) {

			return #[new OpDirectiveNotAllowedException()]
		} else #[]
	}
}
