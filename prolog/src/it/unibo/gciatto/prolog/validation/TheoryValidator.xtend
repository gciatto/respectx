package it.unibo.gciatto.prolog.validation

import it.unibo.gciatto.prolog.prolog.PrologPackage
import it.unibo.gciatto.prolog.prolog.Rule
import it.unibo.gciatto.prolog.prolog.Theory
import it.unibo.gciatto.xtext.validation.utils.ValidationException
import it.unibo.gciatto.prolog.prolog.Variable
import it.unibo.gciatto.prolog.prolog.Number
import it.unibo.gciatto.prolog.prolog.Cut
import it.unibo.gciatto.prolog.prolog.True
import it.unibo.gciatto.prolog.prolog.Fail
import org.eclipse.emf.ecore.EReference

class TheoryValidator {
	static class UnexpectedTermException extends ValidationException {
		
		static val MSG = "Unexpected Term"
		static val CODE = "unexpectedTerm"
	
		new(int index) {
			super(
				Type.ERROR, 
				MSG, 
				PrologPackage.Literals.THEORY__RULES, 
				index,
				CODE
			)
		}
		
		new(int index, EReference ref) {
			super(
				Type.ERROR, 
				MSG, 
				ref,
				index,
				CODE
			)
		}
		
	}
	
	static val TheoryValidator INSTANCE = new TheoryValidator;
	
	static def get() {
		INSTANCE
	}
	
	def Iterable<? extends ValidationException> checkTheory(Theory t) {
		t.rules.checkRuleIterable
	}
	
	def Iterable<? extends ValidationException> checkRuleIterable(Iterable<? extends Rule> rules) {
		rules.indexed
			.map[
				switch (it.value) {
					Variable : checkRule(it.value, it.key)
					Number : checkRule(it.value, it.key)
					Cut : checkRule(it.value, it.key)
					True : checkRule(it.value, it.key)
					Fail : checkRule(it.value, it.key)
					default : null
				}
			].filterNull
	}
	
	protected def checkRule(Rule r, int i) {
		new UnexpectedTermException(i)
	}
}