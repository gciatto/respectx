package it.unibo.gciatto.prolog.validation

import it.unibo.gciatto.prolog.naming.IPrologSimpleNameProvider
import it.unibo.gciatto.prolog.prolog.AnonymousVariable
import it.unibo.gciatto.prolog.prolog.PrologPackage
import it.unibo.gciatto.prolog.prolog.Variable
import it.unibo.gciatto.xtext.validation.utils.ValidationException
import org.eclipse.xtext.scoping.IScope

package class VariablesValidator {
	static class UnusedVariableException extends ValidationException {
		
		static val MSG = "Unused variable"
		static val CODE = "unusedVariable"
	
		new() {
			super(
				Type.WARNING, 
				MSG, 
				PrologPackage.Literals.EXPRESSION__NAME,
				CODE
			)
		}
		
	}
	
	static class VariablesValidionContext {
		val IScope scope;
		val IPrologSimpleNameProvider nameProvider;
//		val List<ValidationException> exceptions = new LinkedList
		
		new (IScope scope, IPrologSimpleNameProvider nameProvider) {
			this.scope = scope
			this.nameProvider = nameProvider
		}
		
//		def putException(ValidationException ex) {
//			exceptions.add(ex)
//		}
//		
//		def List<? extends ValidationException> getExceptions() {
//			exceptions
//		}
	}
	
	static val VariablesValidator INSTANCE = new VariablesValidator;
	
	static def get() {
		INSTANCE
	}
	
	def Iterable<? extends ValidationException> checkVariable(Variable v, VariablesValidionContext c) {
		if (v instanceof AnonymousVariable) {
			return #[]
		} else {
			val count = c.scope.allElements
				.map[it.name.lastSegment]
				.filter[it.equals(v.name)]
				.size
			if (count < 2) 
				#[new UnusedVariableException()]
			else
				#[]
		}
	}
}