package it.unibo.gciatto.prolog.utils

import alice.tuprolog.Theory
import it.unibo.gciatto.prolog.prolog.Expression
import it.unibo.gciatto.prolog.prolog.Term
import it.unibo.gciatto.prolog.prolog.Struct
import it.unibo.gciatto.prolog.prolog.Atom
import it.unibo.gciatto.prolog.prolog.Variable
import it.unibo.gciatto.prolog.prolog.List

class TuprologConversions {
	
	static def dispatch alice.tuprolog.Term toTerm(Expression t) {
		if (t.name != null) {
			if (t.left != null && t.right != null) {
				new alice.tuprolog.Struct(t.name, t.left.toTerm, t.right.toTerm)
			} else if (t.right != null) {
				new alice.tuprolog.Struct(t.name, t.right.toTerm)
			} else if (t.left != null) {
				new alice.tuprolog.Struct(t.name, t.left.toTerm)
			} else {
				null
			}
		} else {
			if (t.left != null) {
				t.left.toTerm
			} else if (t.right != null) {
				t.right.toTerm
			}
		}
	}
	
	static def String apicize(String s){
		val s1 = s.trim
		if (s1.charAt(0) == APIX && s1.charAt(s1.length - 1) == APIX) {
			s1
		} else {
			"\'" + s1 + "\'"
		}
	}
	
	static val APIX = "'".charAt(0)
	
	static def String unapicize(String s){
		val s1 = s.trim
		if (s1.charAt(0) == APIX && s1.charAt(s1.length - 1) == APIX) {
			s1.substring(1, s1.length - 1)
		} else {
			s1
		}
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Term t) {
		return null;
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Struct t) {
		val alice.tuprolog.Term[] temp = newArrayOfSize(0)
		new alice.tuprolog.Struct(t.name.unapicize, t.argument.map[it.toTerm].toArray(temp))
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Atom t) {
		new alice.tuprolog.Struct(t.name.unapicize)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Variable t) {
		new alice.tuprolog.Var(t.name)
	}
	
	static def Number toNumber(it.unibo.gciatto.prolog.prolog.Number t) {
		if (t.intValue != null) {
			return new Integer(t.intValue)
		} else if (t.doubleValue != null) {
			return new Double(t.doubleValue)
		} else {
			return null
		}
	}
	
	static def dispatch alice.tuprolog.Term toTerm(it.unibo.gciatto.prolog.prolog.Number t) {
		alice.tuprolog.Number.createNumber(t.toNumber.toString)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(List t) {
		toList(t.head.map[it.toTerm], if (t.tail == null) null else t.tail.toTerm)
	}
	
	static def asList(alice.tuprolog.Term... terms) {
		terms.toList
	}
	
	static def asTheory(alice.tuprolog.Term... terms) {
		new Theory(terms.asList as alice.tuprolog.Struct)
	}
	
	static def alice.tuprolog.Term toList(Iterable<? extends alice.tuprolog.Term> head) {
		toList(head, null)
	}
	
	static def alice.tuprolog.Term toList(Iterable<? extends alice.tuprolog.Term> head, alice.tuprolog.Term tail) {
		if (head.empty) {
			if (tail == null) {
				new alice.tuprolog.Struct()
			} else {
				tail
			}
		} else {
			new alice.tuprolog.Struct(head.head, toList(head.tail, tail))
		}
	}
	
	static def alice.tuprolog.Term reduceRight(Iterable<? extends alice.tuprolog.Term> terms, String functor) {		
		switch(terms.size) {
			case 0:	throw new IllegalArgumentException
			case 1: terms.head
			case 2: new alice.tuprolog.Struct(functor, terms.head, terms.tail.head) 
			default: new alice.tuprolog.Struct(functor, terms.head, terms.tail.reduceRight(functor)) 
		}
	} 
	
	static def alice.tuprolog.Term conjunction(Iterable<? extends alice.tuprolog.Term> terms) {
		terms.reduceRight(",")
	}	
	
}