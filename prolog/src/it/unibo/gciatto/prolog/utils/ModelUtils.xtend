package it.unibo.gciatto.prolog.utils

import it.unibo.gciatto.prolog.prolog.Term
import it.unibo.gciatto.prolog.prolog.Struct
import it.unibo.gciatto.prolog.prolog.Expression
import java.util.List
import java.util.LinkedList
import it.unibo.gciatto.prolog.prolog.Atom
import it.unibo.gciatto.prolog.prolog.Cut

class ModelUtils {

	static def isStruct(Term t) {
		t instanceof Struct
	}
	
	static def asTerm(Expression e) {
		e as Expression
	}
	
	static def asStruct(Term t) {
		t as Struct
	}
	
	static def getFunctor(Struct s) {
		s.name
	}
	
	static def isExpression(Expression s) {
		s.left != null || s.right != null
	}
	
	static def asExpression(Expression e) {
		e
	}
	
	static def isTerm(Expression e) {
		e instanceof Term
	}
	
	static def getArity(Struct s) {
		s.argument.size
	}
	
	/**
	 * Counts the amount of comma-separated sub-expressions.
	 * E.g. the expression a, b, c, (d, e)
	 * would have depth 4
	 */
	static def int getDepth(Expression e) {
		if (e.expression && e.name == ",") {
			1 + getDepth(e.right)
		} else {
			1
		}
	}
	
	/**
	 * Given an expression of comma-separated expressions
	 * flattens it
	 */
	static def flatten(Expression e) {
		flatten(e, new LinkedList)
	}
	
	private static def List<Expression> flatten(Expression e, List<Expression> accumulator) {
		if (e.expression && e.name == ",") {
			accumulator.add(e.left)
			flatten(e.right, accumulator)
		} else {
			accumulator.add(e)
			accumulator
		}
	}
	
	/**
	 * Given an expression of comma-separated expressions
	 * gets the i-th sub-exp
	 */
	static def get(Expression e, int i) {
		get(e, 0, i)
	}
	
	private static def Expression get(Expression e, int state, int index) {
		if (e.expression && e.name == ",") {
			if (state == index) {
				e.left
			} else {
				get(e.right, state + 1, index)
			}
		} else {
			if (state == index) {
				e
			} else {
				throw new IndexOutOfBoundsException
			}
		}
	}
	
	/**
	 * Check if e has a rh-side
	 */
	static def isPrefix(Expression e) {
		e.right != null && e.left == null
	}
	
	/**
	 * Check if e has a lh-side
	 */
	static def isSuffix(Expression e) {
		e.right == null && e.left != null
	}
	
	/**
	 * Check if e is nested
	 */
	static def hasNested(Expression e) {
		e.nested != null
	}
	
	/**
	 * Check if e has both lh-side and rh-side
	 */
	static def isInfix(Expression e) {
		e.right != null && e.left != null
	}
	
	/**
	 * Check if e has an operator
	 */
	static def hasOperator(Expression e) {
		e.name != null && e.expression
	}
	
	static def getOperator(Expression e) {
		e.name
	}
	
	static def isDirective(Expression e) {
		e.prefix && e.name == ":-"
	}
	
	static def isRule(Expression e) {
		e.name == ":-"
	}
}