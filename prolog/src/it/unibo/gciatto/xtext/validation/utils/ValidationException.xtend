package it.unibo.gciatto.xtext.validation.utils

import java.lang.Exception
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.validation.ValidationMessageAcceptor

class ValidationException extends Exception {
	public enum Type {
		ERROR,
		WARNING	
	}
	
	public val Type type;
	public val EObject source
	public val EStructuralFeature feature
	public val int index
	public val String code
	public val String[] issueData
	
	new (Type type, String message, EObject source, EStructuralFeature feature, int index, String code, String... issueData) {
		super(message)
		this.type = type
		this.source = source
		this.feature = feature
		this.index = index
		this.code = code
		this.issueData = issueData
	}
	
	new (Type type, String message, EObject source, EStructuralFeature feature, String code, String... issueData) {
		this(type, message, source, feature, ValidationMessageAcceptor.INSIGNIFICANT_INDEX, code, issueData)
	}
	
	new (Type type, String message, EStructuralFeature feature, String code, String... issueData) {
		this(type, message, feature, ValidationMessageAcceptor.INSIGNIFICANT_INDEX, code, issueData)
	}
	
	new (Type type, String message, EObject source, EStructuralFeature feature, int index) {
		this(type, message, source, feature, index, null)
	}
	
	new (Type type, String message, EObject source, EStructuralFeature feature) {
		this(type, message, source, feature, ValidationMessageAcceptor.INSIGNIFICANT_INDEX)
	}
	
	new (Type type, String message, EStructuralFeature feature, int index, String code, String... issueData) {
		super(message)
		this.type = type
		this.source = null
		this.feature = feature
		this.index = index
		this.code = code
		this.issueData = issueData
	}
	
	new (Type type, String message, EStructuralFeature feature, int index) {
		this(type, message, feature, index, null)
	}
	
	new (Type type, String message, EStructuralFeature feature) {
		this(type, message, feature, ValidationMessageAcceptor.INSIGNIFICANT_INDEX)
	}
}
