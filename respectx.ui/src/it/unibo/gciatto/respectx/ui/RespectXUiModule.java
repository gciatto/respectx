/*
 * generated by Xtext
 */
package it.unibo.gciatto.respectx.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the IDE.
 */
public class RespectXUiModule extends it.unibo.gciatto.respectx.ui.AbstractRespectXUiModule {
	public RespectXUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
