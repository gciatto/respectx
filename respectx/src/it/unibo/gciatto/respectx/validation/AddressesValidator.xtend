package it.unibo.gciatto.respectx.validation

import it.unibo.gciatto.xtext.validation.utils.ValidationException
import it.unibo.gciatto.prolog.prolog.PrologPackage
import it.unibo.gciatto.respectx.respectX.OnStruct

import static extension java.lang.Integer.parseInt
import it.unibo.gciatto.prolog.prolog.Atom
import java.util.regex.Pattern
import java.net.URL
import java.net.MalformedURLException
import java.util.List
import java.util.LinkedList
import it.unibo.gciatto.respectx.respectX.AtStruct

package class AddressesValidator {
	
//	static val DOT_DECIMAL_NOTATION_PATTERN = Pattern.compile("(\\d{1,3})[.](\\d{1,3})[.](\\d{1,3})[.](\\d{1,3})")
	static val DOT_DECIMAL_NOTATION_PATTERN = Pattern.compile("(\\d{1,3})([.](\\d{1,3})){3,3}")
	
	static class PortException extends ValidationException {
		static val MSG_RESERVED_PORT = "Port numbers in the range 1 .. 1023 may be reserved."
		static val CODE_RESERVED_PORT = "reservedPort"
		static val MSG_INVALID_PORT = "Invalid port. Only variables, 'tucson_port' or integer between 1 and 65535 are admitted."
		static val CODE_INVALID_PORT = "invalidPort"
	
		new(Type t, String m, String c) {
			super(t, m, PrologPackage.Literals.STRUCT__ARGUMENT, 1, c)
		}
		
		static def invalidPort() {
			new PortException(Type.ERROR, MSG_INVALID_PORT, CODE_INVALID_PORT)
		}
		
		static def reservedPort() {
			new PortException(Type.WARNING, MSG_RESERVED_PORT, CODE_RESERVED_PORT)
		}
	}
	
	static class AddressException extends ValidationException {
		
		static val MSG_INVALID_ADDR = "Invalid IP Address or URL"
		static val CODE_INVALID_ADDR = "invalidAddress"
	
		new(Type t, String m, String c) {
			super(t, m, PrologPackage.Literals.STRUCT__ARGUMENT, 0, c)
		}
		
		static def invalidAddress() {
			new PortException(Type.ERROR, MSG_INVALID_ADDR, CODE_INVALID_ADDR)
		}
	}
	
	static val INSTANCE = new AddressesValidator;
	
	static def get() {
		INSTANCE
	}
	
	def Iterable<? extends ValidationException> checkAtStruct(AtStruct t){
		val ctx = new LinkedList<ValidationException>
		val addrArg = t.argument.get(1)
		if (addrArg instanceof Atom) {
			addrArg.name.checkAddress(ctx)	
		}		
		return ctx
	}
	
	def Iterable<? extends ValidationException> checkOnStruct(OnStruct t){
		val ctx = new LinkedList<ValidationException>
		t.checkOnStructAddress(ctx)
		t.checkOnStructPort(ctx)
		return ctx
	}
	
	protected def void checkOnStructAddress(OnStruct t, List<ValidationException> lst) {
		val address = t.argument.get(0)
		if (address instanceof Atom) {
			address.name.checkAddress(lst)
		}
	}
	
	protected def void checkOnStructPort(OnStruct t, List<ValidationException> lst) {
		val port = t.argument.get(1)
		
		if (port instanceof it.unibo.gciatto.prolog.prolog.Number) {
			if (port.doubleValue != null) 
				lst.add(PortException::invalidPort)
			
			port.intValue.checkPort(lst)	
		} else if (port instanceof Atom && port.name != "tucson_port") {
			try {
				port.name.parseInt.checkPort(lst)
			} catch (NumberFormatException e)  {
				lst.add(PortException::invalidPort)
			}
		}
		
		
	}


	protected def void checkPort(int port, List<ValidationException> lst) {
		if (port < 1 || port > ((1 << 16) - 1))
			lst.add(PortException::invalidPort)
		else if (port >= 1 && port < 1024)
			lst.add(PortException::reservedPort)
	}
	
	protected def checkAddress(String addr, List<ValidationException> lst) {
		if ("localhost" != addr) { 
			if (DOT_DECIMAL_NOTATION_PATTERN.matcher(addr).matches) {
				addr.checkIpAddress(lst)
			} else {
				try {
					new URL(addr)
				} catch(MalformedURLException e) {
					lst.add(AddressException::invalidAddress)
				}
			}
		
		}
	}
	
	protected def void checkIpAddress(String addr, List<ValidationException> lst) {
		addr.split("[.]").map[{
			try {
				it.parseInt
			} catch (NumberFormatException e) {
				lst.add(AddressException::invalidAddress)
				return -1
			}
		}].checkFourDottedNotation(lst)
	}
	
	protected def checkFourDottedNotation(Iterable<Integer> nums, List<ValidationException> lst) {
		if (nums.exists[it < 0 || it > 255]) {
			lst.add(AddressException::invalidAddress)
		}
	}
}