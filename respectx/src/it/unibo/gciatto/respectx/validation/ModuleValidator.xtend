package it.unibo.gciatto.respectx.validation

import it.unibo.gciatto.prolog.validation.TheoryValidator
import it.unibo.gciatto.xtext.validation.utils.ValidationException
import it.unibo.gciatto.respectx.respectX.Module
import it.unibo.gciatto.prolog.prolog.Rule
import it.unibo.gciatto.respectx.respectX.RespectXPackage
import it.unibo.gciatto.prolog.prolog.Theory

class ModuleValidator extends TheoryValidator {
	
	static val INSTANCE = new ModuleValidator;
	
	static def ModuleValidator get() {
		INSTANCE
	}
	
	def Iterable<? extends ValidationException> checkModule(Module t) {
		t.rules.checkRuleIterable
	}
	
	override checkTheory(Theory t) {
		#[]
	}
	
	override protected def checkRule(Rule r, int i) {
		new UnexpectedTermException(i, RespectXPackage.Literals.MODULE__RULES)
	}
}