package it.unibo.gciatto.respectx.validation

import it.unibo.gciatto.respectx.respectX.InlineReaction
import it.unibo.gciatto.respectx.respectX.RespectXPackage
import it.unibo.gciatto.xtext.validation.utils.ValidationException
import it.unibo.gciatto.respectx.respectX.ReactionRule

package class SupportValidator {
	
	static class InlineReactionNotSupportedException extends ValidationException {
		
		static val MSG = "Referencing reaction by name is still not supported."
		static val CODE = "inlineReactionReference"
	
		new() {
			super(
				Type.ERROR, 
				MSG, 
				RespectXPackage.Literals.INLINE_REACTION__REFERENCE, 
				CODE
			)
		}
		
	}

	static class VirtualReactionNotSupportedException extends ValidationException {
		
		static val MSG = "Virtual reactions are still not supported. Generated ReSpecT code will be a normal reaction."
		static val CODE = "virtualReaction"
	
		new() {
			super(
				Type.WARNING, 
				MSG, 
				RespectXPackage.Literals.REACTION_RULE__VIRTUAL, 
				CODE
			)
		}
		
	}
	
	static val INSTANCE = new SupportValidator;
	
	static def get() {
		INSTANCE
	}
	
	def Iterable<? extends ValidationException> checkInlineReaction(InlineReaction reac) {
		if (reac.reference != null) 
			#[new InlineReactionNotSupportedException]
		else
			#[]
	} 
	
	def Iterable<? extends ValidationException> checkVirtualReactionRule(ReactionRule rule) {
		if (rule.virtual) 
			#[new VirtualReactionNotSupportedException]
		else
			#[]
	}
}