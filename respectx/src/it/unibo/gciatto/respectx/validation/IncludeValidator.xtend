package it.unibo.gciatto.respectx.validation

import it.unibo.gciatto.respectx.respectX.RespectXPackage
import it.unibo.gciatto.respectx.respectX.IncludeRule
import it.unibo.gciatto.xtext.validation.utils.ValidationException

package class IncludeValidator {
	static class SpecificationIncludeNotAllowedException extends ValidationException {
		static val MSG = "Only not-specification-modules can be included."
		static val CODE = "includeSpecification"
		
		new() {
			super(
				Type.ERROR,
				MSG,
				RespectXPackage.Literals.INCLUDE_RULE__NAME,
				CODE
			)
		}
	}
	
	static val INSTANCE = new IncludeValidator;
	
	static def get() {
		INSTANCE
	}
	
	def Iterable<? extends ValidationException> checkIncludeRule(IncludeRule i) {
		if (i.name.specification) 
			#[new SpecificationIncludeNotAllowedException]
		else
			#[]
	}
}