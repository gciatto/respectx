package it.unibo.gciatto.respectx.validation

import alice.tuprolog.Theory
import alice.tuprolog.Prolog
import alice.tuprolog.Term
import alice.tuprolog.Struct
import alice.tuprolog.Var

class ConflictsHelper {
	
	private static val conflictsTheoryCode = 
		'''
		add(operation) :-
			!, not(operation),
			assertz(operation),
			add(from_agent),
			add(to_tc), !.
		add(internal) :-
			!, not(internal),
			assertz(internal),
			add(from_tc),
			add(to_tc),
			add(endo),
			add(intra), !.
		add(link_in) :-
			!, not(link_in),
			assertz(link_in),
			add(from_tc),
			add(to_tc),
			add(exo),
			add(intra), !.
		add(link_out) :-
			!, not(link_out),
			assertz(link_out),
			add(from_tc),
			add(to_tc),
			add(endo),
			add(inter), !.
		add(X) :- 
			!, not(X), 
			assertz(X).
		
		conflict(completion, invocation) :-
			completion, invocation.
				
		conflict(success, failure) :-
			success, failure.
		
		conflict(endo, exo) :-
			endo, exo.
		
		conflict(intra, inter) :-
			intra, inter.
		
		conflict(from_agent, from_tc) :-
			from_agent, from_tc.
		
		conflict(to_agent, to_tc) :-
			to_agent, to_tc.
		
		conflict(operation, G)	:-
			conflict(from_agent, G), !.
		conflict(operation, G)	:-
			conflict(to_tc, G).

		conflict(internal, G)	:-
			conflict(from_tc, G), !.
		conflict(internal, G)	:-
			conflict(to_tc, G), !.
		conflict(internal, G)	:-
			conflict(endo, G), !.
		conflict(internal, G)	:-
			conflict(intra, G).

		conflict(link_out, G)	:-
			conflict(from_tc, G), !.
		conflict(link_out, G)	:-
			conflict(to_tc, G), !.
		conflict(link_out, G)	:-
			conflict(endo, G), !.
		conflict(link_out, G)	:-
			conflict(inter, G).

		conflict(link_in, G)	:-
			conflict(from_tc, G), !.
		conflict(link_in, G)	:-
			conflict(to_tc, G), !.
		conflict(link_in, G)	:-
			conflict(exo, G), !.
		conflict(link_in, G)	:-
			conflict(intra, G).
		
		conflict(rdp(X), nop(Y)) :-
			rdp(X),
			nop(Y),
			ground(X),
			X = Y.
		
		conflict(inp(X), nop(Y)) :-
			inp(X),
			nop(Y),
			ground(X),
			X = Y.
		
		conflict(after(T1), before(T2)) :-
			after(T1),
			before(T2),
			number(T1),
			number(T2),
			T1 >= T2.
		
		conflict(between(T1, T2), after(T))	:-
			after(T),
			between(T1, T2),
			number(T1),
			number(T2),
			number(T),
			(T1 >= T2;
			T >= T2).
		
		
		conflict(between(T1, T2), before(T)) :-
			before(T),
			between(T1, T2),
			number(T1),
			number(T2),
			number(T),
			(T1 >= T2;
			T =< T1).
		
		conflicts(X, Y) :-
			conflict(X, Y), !.
		
		conflicts(X, Y) :-
			conflict(Y, X).
		'''
		
	private static val conflictsTheory = new Theory(conflictsTheoryCode)
	
	static def getConflictsHelperProlog() {
		new Prolog => [
			theory = conflictsTheory
		]
	}
	
	static def conflicts(Prolog p, Term t) {
		conflicts(p, t, null)
	}
	
	static def conflicts(Prolog p, Term t, String varname) {
		p.solve(new Struct("conflicts", t, if (varname == null) new Var() else new Var(varname)))
	}
	
	static def assertA(Prolog p, Term t) {
		p.theoryManager.assertA(t as alice.tuprolog.Struct, true, null, false)
	}
	
	static def assertZ(Prolog p, Term t) {
		p.theoryManager.assertZ(t as alice.tuprolog.Struct, true, null, false)
	}
	
	static def boolean add(Prolog p, Term t) {
		val addS = new alice.tuprolog.Struct("add", t)
		p.solve(addS).success;
	}
}