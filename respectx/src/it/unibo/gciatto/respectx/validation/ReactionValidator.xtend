package it.unibo.gciatto.respectx.validation

import alice.tuprolog.Prolog
import it.unibo.gciatto.respectx.respectX.AtomicGuard
import it.unibo.gciatto.respectx.respectX.Guard
import it.unibo.gciatto.respectx.respectX.Module
import it.unibo.gciatto.respectx.respectX.Reaction
import it.unibo.gciatto.respectx.respectX.ReactionRule
import it.unibo.gciatto.respectx.respectX.RespectXPackage
import it.unibo.gciatto.respectx.respectX.SpecialGuard
import it.unibo.gciatto.xtext.validation.utils.ValidationException
import java.util.LinkedList
import java.util.List

import static extension it.unibo.gciatto.respectx.utils.TuprologConversions.*
import static extension it.unibo.gciatto.respectx.validation.ConflictsHelper.*
import it.unibo.gciatto.respectx.respectX.TimedGuard
import it.unibo.gciatto.prolog.prolog.Number

package class ReactionValidator {
	
	static class GuardException extends ValidationException {
		
		static val MSG_REPEATED = "Repeated guard"
		static val CODE_REPEATED = "repeatedGuard"
		
		static val MSG_WRONG = "Invalid guard argument"
		static val CODE_WRONG = "wrongGuard"
		
		static val MSG_CONFLICT = "Guard conflicts with other one"
		static val CODE_CONFLICT = "guardConflict"
	
		private new(Guard g, String msg, String code) {
			this(
				switch (g) {
					AtomicGuard: ValidationException.Type.ERROR
					default: ValidationException.Type.WARNING
				}, 
				g,
				msg,
				code
			)
		}
		
		private new(Type t, Guard g, String msg, String code) {
			super(
				t, 
				msg,
				g, 
				switch (g) {
					SpecialGuard: RespectXPackage.Literals.SPECIAL_GUARD__TUPLE
					default: RespectXPackage.Literals.GUARD__NAME
				},
				code
			)
		}
		
		static def repeated(Guard g) {
			new GuardException(g, MSG_REPEATED, CODE_REPEATED)
		}
		
		static def wrong(Guard g) {
			new GuardException(Type.ERROR, g, MSG_WRONG, CODE_WRONG)
		}
		
		static def conflict(Guard g) {
			new GuardException(g, MSG_CONFLICT, CODE_CONFLICT)
		}
	}
	
	static class ReactionRuleException extends ValidationException {
		
		static val MSG = "There exists another reaction triggered by same event & guards"
		static val CODE = "repeatedReaction"
	
		private new(ReactionRule r) {
			super(
				Type.WARNING,
				MSG,
				r,
				RespectXPackage.Literals.REACTION_RULE__REACTION,
				CODE
			)
		}
	}
	
	static class ReactionValidationContext {
		val Prolog prolog
		val List<ValidationException> exceptions
		
		new (Prolog p, List<ValidationException> exs) {
			prolog = p
			exceptions = exs
		}
		
		new (List<ValidationException> exs) {
			this(new Prolog, exs)
		}
		
		new (Prolog p) {
			this(p, new LinkedList)
		}
		
		new () {
			this(new Prolog)
		}
	}
	
	static val INSTANCE = new ReactionValidator;
	
	static def get() {
		INSTANCE
	}
	
	
	def Iterable<? extends ValidationException> checkReactionGuards(Reaction r) {
		val ctx = new ReactionValidationContext(getConflictsHelperProlog)
		r.checkReactionGuards(ctx)
		ctx.exceptions
	}
	
	def Iterable<? extends ValidationException> checkBetween(TimedGuard tg) {
		if (tg.name == "between") {
			val after = tg.instants.get(0)
			val before = tg.instants.get(1)
			if (after instanceof Number && before instanceof Number){
				if ((after as Number).intValue >= (before as Number).intValue) {
					#[GuardException::wrong(tg)]
				}
			}
		}
		#[]
	}
	
	def Iterable<? extends ValidationException> checkTimedGuard(TimedGuard tg) {
		if (tg.instants.filter(Number).exists[it.intValue == null]) 
			#[GuardException::wrong(tg)]
		else
			#[]
	}
	
	protected def checkReactionGuards(Reaction r, ReactionValidationContext ctx) {
		if (r.guards == null || r.guards.empty) {
			// do nothing
		} else {
			r.guards.forEach[it.checkGuard(ctx)]
		}
	}
	
	protected def checkGuard(Guard g, ReactionValidationContext ctx) {
		checkGuardNotRepeated(g, ctx)
		checkGuardNotConflict(g, ctx)
	}
	
	protected def checkGuardNotRepeated(Guard g, ReactionValidationContext ctx) {
		val term = g.toTerm
		val si = ctx.prolog.solve(term)
		if (si.success) {
			ctx.exceptions.add(GuardException::repeated(g))
		} else {
			if (ctx.prolog.add(term)) {
				
			} else {
				ctx.exceptions.add(GuardException::repeated(g))
			}
		}
	}
	
	protected def checkGuardNotConflict(Guard g, ReactionValidationContext ctx) {
		val term = g.toTerm
		val si = ctx.prolog.conflicts(term)
		if (si.success) 
			ctx.exceptions.add(GuardException::conflict(g))
	}
	
	def Iterable<? extends ValidationException> checkModuleReactions(Module m) {
		val ctx = new ReactionValidationContext()
		m.checkModuleReactions(ctx)
		ctx.exceptions
	}
	
	protected def checkModuleReactions(Module m, ReactionValidationContext ctx) {
		m.rules
			.filter(ReactionRule)
			.forEach[
				val rSignature = it.signature
				val si = ctx.prolog.solve(rSignature)
				if (si.success) {
					ctx.exceptions.add(new ReactionRuleException(it))
				} else {
					ctx.prolog.assertZ(rSignature)
				}
			]
	}
}