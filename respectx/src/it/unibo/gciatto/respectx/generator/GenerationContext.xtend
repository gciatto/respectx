package it.unibo.gciatto.respectx.generator

import java.util.Set
import it.unibo.gciatto.respectx.respectX.Module
import java.util.HashSet
import java.util.Stack

package class GenerationContext {
	static val NO_SPACE_BEFORE_OPERATORS = #{
		",",
		";"
	}
	
	static val NO_SPACE_AFTER_OPERATORS = #{
	}
	
	static val LINE_WRAP_AFTER_OPERATORS = #{
		",",
		";",
		"->"
	}
	
	private val insideRuleBody = new Stack<Integer>()
	
	
	public val Set<Module> alreadyGeneratesModules = new HashSet()
	
	public val noSpaceBeforeOperators = NO_SPACE_BEFORE_OPERATORS
	
	public val noSpaceAfterOperators = NO_SPACE_AFTER_OPERATORS
	
	public val lineWrapAfterOperators = LINE_WRAP_AFTER_OPERATORS
	
	def boolean isInsideRuleBody() {
		insideRuleBody.size > 0
	}
	
	def void setInsideRuleBody(boolean value) {
		if (value) {
			insideRuleBody.push(insideRuleBody.size + 1);
		} else {
			insideRuleBody.pop
		}
	}
	
}