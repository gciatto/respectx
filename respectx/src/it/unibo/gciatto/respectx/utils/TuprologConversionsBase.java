package it.unibo.gciatto.respectx.utils;

import it.unibo.gciatto.respectx.respectX.Reaction;

/**
 * Since Xtend makes bitwise operations a pain
 * this class contains flags for {@link TuprologConversions#toTerms(Reaction, int)}
 * 
 * @author gciatto
 *
 */
class TuprologConversionsBase {
	public static final int R_NONE = 0;
	public static final int R_EVENT = 1;
	public static final int R_GUARD = 2;
	public static final int R_BODY = 4;
	public static final int R_ALL = R_EVENT | R_GUARD | R_BODY;
	public static final int R_GUARDLIST = 16;
	public static final int R_SIGNATURE = R_EVENT | R_GUARD | R_GUARDLIST;

	public static boolean isNone(int flag) {
		return flag == 0;
	}

	public static boolean isEvent(int flag) {
		return (flag & R_EVENT) > 0;
	}

	public static boolean isGuard(int flag) {
		return (flag & R_GUARD) > 0;
	}

	public static boolean isBody(int flag) {
		return (flag & R_BODY) > 0;
	}

	public static boolean isAll(int flag) {
		return (flag & R_ALL) > 0;
	}

	public static boolean isGuardList(int flag) {
		return (flag & R_GUARDLIST) > 0;
	}

	public static boolean isSignature(int flag) {
		return (flag & R_SIGNATURE) > 0;
	}
}
