package it.unibo.gciatto.respectx.utils

import it.unibo.gciatto.respectx.respectX.Guard
import it.unibo.gciatto.respectx.respectX.TimedGuard

import it.unibo.gciatto.respectx.respectX.ExistenceGuard
import it.unibo.gciatto.respectx.respectX.AbsenceGuard

import it.unibo.gciatto.prolog.prolog.Term
import it.unibo.gciatto.prolog.prolog.Expression
import it.unibo.gciatto.respectx.respectX.Reaction

import static extension com.google.common.collect.Iterables.concat;
import it.unibo.gciatto.respectx.respectX.Event
import it.unibo.gciatto.respectx.respectX.ReactionRule
import it.unibo.gciatto.respectx.respectX.SpecialGuard
import it.unibo.gciatto.respectx.respectX.ListReturningEvent
import alice.tuprolog.Var

class TuprologConversions extends TuprologConversionsBase {
	
	static def dispatch alice.tuprolog.Term toTerm(Term t) {
		it.unibo.gciatto.prolog.utils.TuprologConversions.toTerm(t)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Expression e) {
		it.unibo.gciatto.prolog.utils.TuprologConversions.toTerm(e)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Guard g) {
		new alice.tuprolog.Struct(g.name)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(TimedGuard g) {
		val alice.tuprolog.Term[] args = newArrayOfSize(0)
		
		new alice.tuprolog.Struct(g.name, g.instants.map[it.toTerm].toArray(args))
	}
	
	static def dispatch alice.tuprolog.Term toTerm(ExistenceGuard g) {
		new alice.tuprolog.Struct("rdp", g.tuple.toTerm)
	}
	
//	static def dispatch alice.tuprolog.Term toTerm(ExistenceRemovalGuard g) {
//		new alice.tuprolog.Struct("inp", g.tuple.toTerm)
//	}
	
	static def dispatch alice.tuprolog.Term toTerm(AbsenceGuard g) {
		new alice.tuprolog.Struct("nop", g.tuple.toTerm)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(ReactionRule rr) {
		val tt = rr.reaction.toTerms(R_ALL)
		new alice.tuprolog.Struct(
			"reaction", 
			tt.head,
			tt.tail.head,
			tt.tail.tail.head
		)
	}
	
	/**
	 * This function is used by other functions inside here to make their job.
	 * It is a general-purpose way to extract tuProlog terms from a Reaction object
	 * 
	 * @param flags please use {@link TuprologConversionsBase}.*Constants* here, they can be ORed 
	 */
	static def Iterable<? extends alice.tuprolog.Term> toTerms(Reaction r, int flags) {
		var Iterable<? extends alice.tuprolog.Term> iter = #[]
		
		if (flags.event) {
			iter = iter.concat(#[r.event.toTerm])
		}
		
		if (flags.guard) {
			iter = iter.concat(#[r.guards.toTerms(flags.guardList)])
		}
		
		if (flags.body) {
			iter = iter.concat(#[r.body.toTerm])
		}
		
		return iter
	}
	
	static def dispatch alice.tuprolog.Term toTerm(Event e) {
		new alice.tuprolog.Struct(e.primitive.name, e.tuple.toTerm)
	}
	
	static def dispatch alice.tuprolog.Term toTerm(ListReturningEvent e) {
		val resultTerm = if (e.^return === null) new Var() else e.^return.toTerm
		new alice.tuprolog.Struct(e.primitive.name, e.tuple.toTerm, resultTerm)
	}
	
	protected static def alice.tuprolog.Term toTerms(Iterable<? extends Guard> e, boolean list) {
		val terms = e.filter[!(it instanceof SpecialGuard)]
			.map[it.name]
			.concat(#["true"])
			.sortWith[a, b | a.compareTo(b)]
			.map[new alice.tuprolog.Struct(it)]
		if (list) {
			it.unibo.gciatto.prolog.utils.TuprologConversions.toList(terms)
		} else {
			it.unibo.gciatto.prolog.utils.TuprologConversions.conjunction(terms)
		}
	}
	
	static def alice.tuprolog.Term signature(ReactionRule rr) {
		val tt = rr.reaction.toTerms(R_SIGNATURE)
		new alice.tuprolog.Struct(
			"reaction", 
			tt.head,
			tt.tail.head
		)
	}
}