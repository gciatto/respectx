package it.unibo.gciatto.respectx.naming

import it.unibo.gciatto.prolog.naming.PrologSimpleNameProvider
import org.eclipse.emf.ecore.EObject
import it.unibo.gciatto.prolog.prolog.Variable
import it.unibo.gciatto.respectx.respectX.ReactionRule
import org.eclipse.xtext.naming.QualifiedName

class RespectXSimpleNameProvider extends PrologSimpleNameProvider {
	
	override getFullyQualifiedName(EObject obj) {
		switch(obj) {
			Variable: obj.qualifiedName
			ReactionRule: obj.qualifiedName
			default: obj.qualifiedName
		}
	}
	
	def QualifiedName getQualifiedName(ReactionRule rr) {
		'''reaction/3'''.toQualifiedName
	}
	
}